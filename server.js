var app = require('express')();


require('./app/config/server.js')(app);
require('./app/routes/dashboard.js')(app);

app.listen(8080);
module.exports = function (grunt) {

    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src',
                    src: ['*.scss'],
                    dest: 'public/assets/css',
                    ext: '.css'
                }]
            }
        },
        bowercopy: {
            options: {
                    srcPrefix: 'bower_components'
                },
            scripts: {
                options: {
                    destPrefix: 'public/assets/js/vendor'
                },
                files: {
                    'angular.min.js': 'angular/angular.min.js',
                }
            },
            css: {
                options: {
                    destPrefix: 'public/assets/css/vendor'
                },
                files: {
                    'bootstrap.min.css': 'bootstrap/dist/css/bootstrap.css'
                }
            }
        },
        watch: {
            scripts: {
                files: ['bower.json', 'src/**/*'],
                tasks: ['default'],
                options: {
                    spawn: false,
                },
            },
        },
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['bowercopy', 'sass']);

}
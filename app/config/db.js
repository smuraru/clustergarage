var host = 'localhost',
    port = 27017,
    username = '',
    password = '',
    dbName = 'ClusterGarage';

var createConnectionUrl = function (username, password, host, port, dNname) {
    var connUrl = 'mongodb://';
    
    if (username.length != 0 && password.length != 0) {
        connUrl += username + ':' + password + '@';
    }   //only add username and password to connUrl if they are not empty
    
    connUrl += host + ':' + port + '/' + dbName;

    return connUrl;
}

module.exports = {

    connectionUrl : createConnectionUrl(username, password, host, port, dbName),

    credentials : {

        username : username,
        password : password

    }

};
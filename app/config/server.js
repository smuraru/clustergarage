var express = require('express');
var mongoose = require('mongoose'),
    dbConfig = require('./db.js');

module.exports = function (app) {

    mongoose.connect(dbConfig.connectionUrl, function (err) {
        if (err) {
            console.error(err);
            return;
        }
    });

    app.use(express.static('./public'));

}
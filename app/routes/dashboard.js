var router = require('express').Router();
var path = require('path');

module.exports = function (app) {

    router.get('/', function (req, res) {
        res.sendFile(path.join(__dirname + '/../../public/views/index.html'));
    })

    app.use('/', router);

}